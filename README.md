# Evenlights Hypervisor Tools

Hypervisor tools are mainly about regular VM backups and are meant for privately run clouds and
environments where no out-of-the-box backup solution is available.

The CLI interface is organized in command groups comprising functionality for respective
modules.

### USAGE:
    hvtools [FLAGS] [OPTIONS] [SUBCOMMAND]

### FLAGS:
    -d, --daemon     Run as a system daemon
    -h, --help       Prints help information
    -V, --version    Prints version information

### OPTIONS:
    -c, --config-path <PATH>    Specifies path to load config from [default: /etc/evenlights/hv-tools.json]

### SUBCOMMANDS:
    archive    Contains subcommands to manage machine archives
    help       Prints this message or the help of the given subcommand(s)
    libvirt    Contains subcommands to interact with the libvirt library


## Libvirt

Contains subcommands to interact with the libvirt library

### USAGE:
    hvtools libvirt [OPTIONS] [SUBCOMMAND]

### FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

### OPTIONS:
    -c, --config-path <PATH>    Specifies path to load config from [default: /etc/evenlights/hv-tools.json]

### SUBCOMMANDS:
    archive    Create an archive of all the machine's drives under the root configured in the settings. All the VM
               drives will be stored within a single archive with current timestamp.
    help       Prints this message or the help of the given subcommand(s)


## Archive

Contains subcommands to manage machine archives

### USAGE:
    hvtools archive [OPTIONS] [SUBCOMMAND]

### FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

### OPTIONS:
    -c, --config-path <PATH>    Specifies path to load config from [default: /etc/evenlights/hv-tools.json]

### SUBCOMMANDS:
    cleanup    Cleanup expired archives, that is all the archives older than the value configured in the settings or
               passed by the user.
    help       Prints this message or the help of the given subcommand(s)
