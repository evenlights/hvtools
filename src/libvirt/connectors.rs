use serde::{Deserialize, Serialize};
use serde_xml_rs;
use virt;
use crate::core::GenericError;

pub struct LibvirtConnector {
    conn: virt::connect::Connect,
}

impl LibvirtConnector {
    pub fn new(uri: &str) -> Result<Self, GenericError> {
        debug!("Connecting to libvirt at: {}", uri);
        let conn = virt::connect::Connect::open(uri)?;

        let mut connector = LibvirtConnector { conn };

        debug!("Checking libvirt connection");
        match connector.conn.get_uri() {
            Ok(v) => debug!("Connected to libvirt at: {}", v),
            Err(e) => {
                connector.disconnect()?;
                return Err(GenericError { details: format!("{}", e) });
            }
        };
        debug!("Connected to libvirt at: {}", uri);

        Ok(connector)
    }

    pub fn disconnect(&mut self) -> Result<(), GenericError> {
        debug!("Disconnecting from libvirt");
        self.conn.close()?;

        Ok(())
    }

    pub fn get_domains(&mut self) -> Result<Vec<virt::domain::Domain>, GenericError> {
        // Return a list of all active and inactive domains. Using this API instead of
        // virConnectListDomains() and virConnectListDefinedDomains() is preferred since it
        // "solves" an inherit race between separated API calls if domains are started or stopped
        // between calls
        debug!("Getting full domain list");
        let list_flags = virt::connect::VIR_CONNECT_LIST_DOMAINS_ACTIVE
            | virt::connect::VIR_CONNECT_LIST_DOMAINS_INACTIVE;

        Ok(self.conn.list_all_domains(list_flags)?)
    }

    pub fn get_domain_drives(&mut self, domain: &virt::domain::Domain) -> Result<Vec<String>, GenericError> {
        debug!("Getting full domain drive path list");
        let xml = domain.get_xml_desc(0)?;
        let dom_parsed: LVDomain = serde_xml_rs::from_str(&xml)?;

        Ok(dom_parsed
            .devices
            .disks
            .iter()
            .map(|v| String::from(&v.source.file))
            .collect::<Vec<String>>())
    }
}

#[derive(Debug, Serialize, Deserialize)]
struct LVDomain {
    name: String,
    devices: LVDevices,
}

#[derive(Debug, Serialize, Deserialize)]
struct LVDevices {
    #[serde(rename = "disk", default)]
    disks: Vec<LVDisk>,
}

#[derive(Debug, Serialize, Deserialize)]
struct LVDisk {
    source: LVSource,
}

#[derive(Debug, Serialize, Deserialize)]
struct LVSource {
    file: String,
}

impl From<virt::error::Error> for GenericError {
    fn from(err: virt::error::Error) -> Self {
        GenericError::new(&format!("{}", err))
    }
}

impl From<serde_xml_rs::Error> for GenericError {
    fn from(err: serde_xml_rs::Error) -> Self {
        GenericError::new(&format!("{}", err))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    // Note that this requires libvirtd to be up

    #[test]
    fn test_libvirt_connector_libvirt_connector_new() {
        let mut connector = LibvirtConnector::new("test:///default").unwrap();
        let uri = connector.conn.get_uri().unwrap();
        connector.conn.close().unwrap();

        assert_eq!(uri, "test:///default");
    }

    #[test]
    fn test_libvirt_connector_libvirt_connector_disconnect() {
        let mut connector = LibvirtConnector::new("test:///default").unwrap();
        match connector.disconnect() {
            Ok(_) => {},
            Err(e) => panic!("Could not disconnect from libvirt: {}", e)
        }
    }

    #[test]
    fn test_libvirt_connector_libvirt_connector_get_domains() {
        let mut connector = LibvirtConnector::new("test:///default").unwrap();
        let domains = connector.get_domains().unwrap();
        connector.disconnect().unwrap();

        assert_eq!(domains.len(), 1);
    }

    #[test]
    fn test_libvirt_connector_libvirt_connector_get_domain_drives() {
        let mut connector = LibvirtConnector::new("test:///default").unwrap();
        let domains = connector.get_domains().unwrap();
        let domain0_drives = connector.get_domain_drives(&domains[0]).unwrap();
        connector.disconnect().unwrap();

        assert_eq!(domain0_drives.len(), 1);
        assert_eq!(domain0_drives[0], "/guest/diskimage1");
    }
}
