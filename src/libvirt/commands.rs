use clap;
use clap_nested::{Command, Commander, MultiCommand};
use crate::app::Application;

pub fn get_libvirt_menu<'a>() -> MultiCommand<'a, str, str> {
    let archive_cmd = Command::new("archive")
        .description("Create an archive of all the machine's drives under the root \
            configured in the settings. All the VM drives will be stored within a single archive \
            with current timestamp.")
        .options(|app| {
            app.arg(
                clap::Arg::with_name("debug")
                    .short("d")
                    .help("Prints debug information verbosely"),
            )
        })
        .runner(archive);

    Commander::new()
        .add_cmd(archive_cmd)
        .into_cmd("libvirt")
        .description("Contains subcommands to interact with the libvirt library")
}

fn archive(_args: &str, _matches: &clap::ArgMatches<'_>) -> Result<(), clap::Error> {
    Application::new(None)?.libvirt_archive()?;

    Ok(())
}
