use std::time::Duration;
use clap::{App, Arg, ArgMatches};
use clap_nested::Commander;
use env_logger::Env;
use crate::core::{Settings, GenericError, DEFAULT_CONFIG_LOCATION};
use crate::{archive, libvirt};

pub struct Application {
    pub global_args: GlobalArgs,
    pub settings: Settings,
}

impl Application {
    pub fn get_global_args() -> GlobalArgs {
        let matches = App::new(env!("CARGO_PKG_DESCRIPTION"))
            .version(env!("CARGO_PKG_VERSION"))
            .author(env!("CARGO_PKG_AUTHORS"))
            .about("Does awesome things")

            .arg(Arg::with_name("daemon")
                .short("d")
                .long("daemon")
                .help("Run as a system daemon"))

            .arg(
                Arg::with_name("config-path")
                    .short("c")
                    .long("config-path")
                    .default_value(DEFAULT_CONFIG_LOCATION)
                    .global(true)
                    .takes_value(true)
                    .value_name("CONFIG_PATH")
                    .help("Specifies path to load config from, \
                              defaults to /etc/evenlights/hv-tools.json"))
            .get_matches_safe()
            .ok();

        let run_mode = match &matches {
            Some(v) => match v.occurrences_of("daemon") {
                0 => RunMode::CLI,
                _ => RunMode::Daemon
            },
            None => RunMode::CLI,
        };
        let config_path = match &matches {
            Some(v) => match v.value_of("config-path") {
                Some(v) => v,
                None => DEFAULT_CONFIG_LOCATION
            },
            None => DEFAULT_CONFIG_LOCATION,
        }.to_string();

        GlobalArgs { run_mode, config_path }
    }

    pub fn bootstrap() {
        env_logger::from_env(Env::default().default_filter_or("info")).init();

        info!("Evenlights Hypervisor Tools v{}", env!("CARGO_PKG_VERSION"));
    }

    pub fn cli<'a>() -> Commander<'a, (), str> {
        Self::bootstrap();

        Commander::new()
            .options(|app| {
                app
                    .version(env!("CARGO_PKG_VERSION"))
                    .author("mcaffee <development@evenlights.com>")
                    .about("Evenlights Hypervisor Tools")
                    .arg(
                        Arg::with_name("config-path")
                            .short("c")
                            .long("config-path")
                            .default_value(DEFAULT_CONFIG_LOCATION)
                            .global(true)
                            .takes_value(true)
                            .value_name("PATH")
                            .help("Specifies path to load config from"))
                    .arg(Arg::with_name("daemon")
                        .short("d")
                        .long("daemon")
                        .help("Run as a system daemon"))
            })
            .args(|_args, matches: &ArgMatches<'_>| matches.value_of("config-path").unwrap_or("any"))

            .add_cmd(archive::get_archive_menu())
            .add_cmd(libvirt::get_libvirt_menu())

            .no_cmd(|_args, _matches| {
                println!("No subcommand matched");
                Ok(())
            })
    }

    pub fn daemon() {
        println!("Daemon mode");
        std::process::exit(0);
    }

    pub fn new(global_args: Option<GlobalArgs>) -> Result<Self, GenericError> {
        let global_args = match global_args {
            Some(v) => v,
            None => Self::get_global_args()
        };
        let settings = Settings::new(Some(&global_args.config_path))?.validate()?;

        Ok(Self { global_args, settings })
    }

    pub fn libvirt_archive(&self) -> Result<(), GenericError> {
        info!("Running libvirt archive command");
        let mut connector = libvirt::LibvirtConnector::new(&self.settings.libvirt.address)?;
        let domains = connector.get_domains()?;

        for dom in domains {
            let name = dom.get_name().map_err(|e| GenericError::from(&e))?;
            let id = dom.get_id().unwrap_or(0);
            let active = dom.is_active().map_err(|e| GenericError::from(&e))?;

            info!("Processing domain: name = {}, id = {}, active = {}", name, id, active);

            let mut archive = archive::TarArchive::new_for_machine(&self.settings, &name)?;
            let drive_paths = connector.get_domain_drives(&dom)?;

            for drive_path in drive_paths {
                archive.add_file(&drive_path)?
            }
        }

        info!("Libvirt domains successfully archived");
        Ok(())
    }

    pub fn archive_cleanup(&self, period_after: &Option<Duration>) -> Result<(), GenericError> {
        info!("Running archive cleanup command");
        let period_after = match period_after {
            Some(v) => v,
            None => {
                if self.settings.cleanup_after.as_secs() == 0 {
                    return Err(GenericError::new(
                        "Retention duration is neither configured in the config file nor passed by the \
                        user in the command line, cleaning up not possible."
                    ));
                }

                &self.settings.cleanup_after
            }
        };

        debug!("Cleanup period: {:?}", period_after);

        let registry = archive::ArchiveRegistry::new(&self.settings)?;
        registry.cleanup(chrono::Duration::seconds(period_after.as_secs() as i64))?;

        info!("Archives were successfully cleaned up");
        Ok(())
    }
}

pub struct GlobalArgs {
    pub run_mode: RunMode,
    pub config_path: String,
}

pub enum RunMode {
    CLI,
    Daemon,
}

impl From<GenericError> for clap::Error {
    fn from(e: GenericError) -> Self {
        clap::Error::with_description(
            &format!("{}", e.details),
            clap::ErrorKind::InvalidValue)
    }
}
