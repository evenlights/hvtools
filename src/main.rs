//! # Evenlights Hypervisor Tools
//!
//! Hypervisor tools are mainly about regular VM backups and are meant for privately run clouds and
//! environments where no out-of-the-box backup solution is available.
//!
//! The CLI interface is organized in command groups comprising functionality for respective
//! modules.
//!
//! ## Libvirt
//!
//! This module provides interface to `libvirt` hypervisor.
//!
//! ### Archive the machines
//!
//! This command will create an archive of all the machine's drives under a root configured in the
//! [settings](core::settings::Settings). All the VM drives will be stored within a single archive
//! with current timestamp.
//!
//! ```shell script
//! $ hvtools libvirt archive
//! ```
extern crate chrono;
#[macro_use]
extern crate log;
#[macro_use]
extern crate clap;
extern crate fake;
extern crate flate2;
extern crate serde_xml_rs;
extern crate tar;
extern crate virt;

pub mod core;
pub mod archive;
pub mod libvirt;
pub mod app;

use crate::app::Application;

fn main() {
    let global_args = Application::get_global_args();

    match &global_args.run_mode {
        app::RunMode::CLI => {
            let cli = Application::cli();

            match cli.run() {
                Ok(_) => (),
                Err(e) => println!("{}", e),
            };
        },

        app::RunMode::Daemon => {
            Application::daemon()
        },
    }
}

