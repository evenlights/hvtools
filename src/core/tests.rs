use std::fs;
use fake::{Faker, Fake};
use chrono::{DateTime, Utc};
use fake::faker::lorem::en::Word;
use crate::archive::ARCHIVE_TIMESTAMP_FORMAT;

pub const CUSTOM_SETTINGS_PATH: &str = "/tmp/hvtools_custom_settings.json";

pub fn init_logging() {
    let _ = env_logger::builder().is_test(true).try_init();
}

pub struct FakeSettingsFile {
    pub path: String
}

impl FakeSettingsFile {
    pub fn new(file_data: &str) -> Self {
        let unique_id: u8 = Faker.fake();
        let path = format!("/tmp/hvtools_custom_settings_{}.json", unique_id);

        match fs::write(&path, file_data) {
            Ok(_) => {},
            Err(e) => panic!(
                "Could not create custom settings file under '{}': {}",
                CUSTOM_SETTINGS_PATH, e
            ),
        };

        Self { path }
    }
}

impl Drop for FakeSettingsFile {
    fn drop(&mut self) {
        match fs::remove_file(&self.path) {
            Ok(_) => {},
            Err(e) => panic!(
                "Could not delete custom settings file under '{}': {}",
                CUSTOM_SETTINGS_PATH, e
            ),
        };
    }
}

pub struct FakeMachineArchive {
    pub path: String
}

impl FakeMachineArchive {
    pub fn new(timestamp: DateTime<Utc>) -> Self {
        let machine_name = format!("{}-{}", Word().fake::<String>(), Faker.fake::<u8>());
        let path = format!(
            "{}/{}-{}.tar.gz",
            "/tmp",
            machine_name,
            timestamp.format(ARCHIVE_TIMESTAMP_FORMAT),
        );

        match fs::write(&path, "") {
            Ok(_) => {},
            Err(e) => panic!(
                "Could not create fake machine archive under '{}': {}",
                path, e
            ),
        };

        Self { path }
    }
}

impl Drop for FakeMachineArchive {
    fn drop(&mut self) {
        match fs::remove_file(&self.path) {
            Ok(_) => {},
            Err(e) => warn!(
                "Could not delete fake machine archive under '{}': {}",
                &self.path, e
            ),
        };
    }
}
