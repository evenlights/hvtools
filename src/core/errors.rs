use std::fmt;
use std::error::Error;

#[derive(Debug)]
pub struct GenericError {
    pub details: String
}

impl GenericError {
    pub fn new(msg: &str) -> Self {
        Self { details: msg.to_string() }
    }

    pub fn from(err: &impl fmt::Display) -> Self {
        Self::new(&format!("{}", err))
    }
}

impl fmt::Display for GenericError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.details)
    }
}

impl Error for GenericError {
    fn description(&self) -> &str {
        &self.details
    }
}
