use std::{fs, path};
use std::time::Duration;
use serde::{Deserialize, Serialize};
use serde_json;
use serde_json::Error;
use faccess::{AccessMode, PathExt};
use crate::core::GenericError;


pub static DEFAULT_CONFIG_LOCATION: &str = "/etc/evenlights/hv-tools.json";

#[derive(Serialize, Deserialize, Debug)]
#[serde(default)]
pub struct Settings {
    // Where to load settings from .This parameter can be overridden via a command-line parameter.
    pub settings_path: String,

    // Not used right now but the idea is to calculate some flow hash and store flow state. E.g.
    // if upload to cloud storage fails it doesn't mean the service should restart the whole
    // process again.
    pub tmp_path: String,

    // Usually it's not a good idea to store files under `/tmp` because its the size of `tmpfs` is
    // usually reasonably limited. Since we're creating relatively large archives it definitely
    // makes sense to move the location somewhere else where this limitation does not exist.
    pub archive_path: String,

    #[serde(with = "humantime_serde")]
    pub cleanup_after: Duration,

    // List of machines to archive. If empty, all tha machines found are archived. Otherwise, only
    // those listed in this field.
    pub machines: Vec<String>,

    // Libvirt settings.
    pub libvirt: LibvirtSettings,
}

impl Default for Settings {
    fn default() -> Self {
        Settings {
            settings_path: DEFAULT_CONFIG_LOCATION.to_string(),
            tmp_path: "/tmp".to_string(),
            archive_path: "/tmp".to_string(),
            cleanup_after: Duration::new(0, 0),
            machines: vec![],
            libvirt: LibvirtSettings {
                address: "qemu:///system".to_string(),
            },
        }
    }
}

impl Settings {
    pub fn new(path: Option<&str>) -> Result<Self, GenericError> {
        let target_path = match path {
            Some(v) => v,
            None => DEFAULT_CONFIG_LOCATION,
        };

        debug!("Reading settings at: {}", target_path);
        let data = fs::read_to_string(target_path)?;

        debug!("Parsing settings");
        let mut settings: Settings = serde_json::from_str(&data)?;
        settings.settings_path = target_path.into();

        let settings_pretty = serde_json::to_string_pretty(&settings)?;
        debug!("Target values: \n{}", settings_pretty);

        Ok(settings)
    }

    pub fn validate(self) -> Result<Self, GenericError> {
        debug!("Verifying settings");

        let tmp_path = path::Path::new(&self.tmp_path);

        match tmp_path.exists() {
            true => if !tmp_path.access(AccessMode::WRITE).is_ok() {
                    return Err(GenericError::new(&format!("Temporary path os not writable: {}", &self.tmp_path)))
                },
            false => {
                debug!("Temporary path doesn't exist, creating: {}", &self.tmp_path);
                fs::create_dir_all(tmp_path)?;
            }
        }

        let archive_path = path::Path::new(&self.archive_path);

        match archive_path.exists() {
            true => if !archive_path.access(AccessMode::WRITE).is_ok() {
                return Err(GenericError::new(&format!("Archive root path os not writable: {}", &self.tmp_path)))
            },
            false => {
                debug!("Archive root path doesn't exist, creating: {}", &self.archive_path);
                fs::create_dir_all(archive_path)?;
            }
        }

        Ok(self)
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct LibvirtSettings {
    pub address: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct DaemonSettings {
    pub pid_file: String,
    pub chown_pid_file: bool,
    pub working_directory: String,
    pub user: String,
    pub group: String,
    pub stdout: String,
    pub stderr: String,
}

impl From<serde_json::Error> for GenericError {
    fn from(e: Error) -> Self {
        Self::from(&e)
    }
}

#[cfg(test)]
mod tests {
    use chrono;
    use crate::core::tests::FakeSettingsFile;
    use super::*;

    #[test]
    fn test_core_settings_settings_new() {
        let settings_helper = FakeSettingsFile::new(
            r#"{
                "tmp_path": "/custom/tmp/path",
                "archive_path": "/custom/archive/tmp/path",
                "machines": ["machine1", "machine2"],
                "libvirt": {
                        "address": "custom:///address"
                    }
            }"#,
        );

        let settings = Settings::new(Some(&settings_helper.path)).unwrap();

        assert_eq!(settings.settings_path, settings_helper.path);
        assert_eq!(settings.tmp_path, "/custom/tmp/path");
        assert_eq!(settings.archive_path, "/custom/archive/tmp/path");
        assert_eq!(settings.machines, vec!["machine1", "machine2"]);
        assert_eq!(settings.libvirt.address, "custom:///address");
    }

    #[test]
    fn test_core_settings_settings_cleanup_after() {
        let settings_helper = FakeSettingsFile::new(
            r#"{
                "cleanup_after": "3 days"
            }"#,
        );

        let settings = Settings::new(Some(&settings_helper.path)).unwrap();

        assert_eq!(
            chrono::Duration::seconds(settings.cleanup_after.as_secs() as i64),
            chrono::Duration::days(3)
        );

        let settings_helper = FakeSettingsFile::new(
            r#"{
            }"#,
        );

        let settings = Settings::new(Some(&settings_helper.path)).unwrap();

        assert_eq!(settings.cleanup_after.as_secs(), 0);
    }

    #[test]
    pub fn test_core_settings_settings_validate() {
        let settings_helper = FakeSettingsFile::new(
            r#"{
                "tmp_path": "/tmp",
                "archive_tmp_path": "/tmp"
            }"#,
        );
        let settings = Settings::new(Some(&settings_helper.path)).unwrap();

        settings.validate().unwrap();
    }
}
