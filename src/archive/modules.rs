use std::{io};
use std::fs::{self, File};
use regex::Regex;
use chrono::{DateTime, Utc, NaiveDateTime, Duration};
use flate2::Compression;
use flate2::write::GzEncoder;

use crate::core::{Settings, GenericError};

pub const ARCHIVE_TIMESTAMP_FORMAT: &str = "%Y%m%d-%H%M";

pub struct TarArchive {
    pub path: String,
    tar: tar::Builder<GzEncoder<File>>,
}

impl TarArchive {
    pub fn new_for_machine(settings: &Settings, machine_name: &str) -> Result<Self, io::Error> {
        let path = TarArchive::get_archive_path(settings, machine_name);
        debug!("Creating archive at: {}", &path);

        let tar_gz = File::create(&path)?;
        let enc = GzEncoder::new(tar_gz, Compression::default());
        let tar = tar::Builder::new(enc);

        Ok(Self { path, tar })
    }
    pub fn add_file(&mut self, path: &str) -> Result<(), io::Error> {
        debug!("Adding path to archive: {}", path);
        return self
            .tar
            .append_file(Self::get_relative_path(path), &mut File::open(path)?);
    }

    fn get_archive_path(settings: &Settings, machine_name: &str) -> String {
        let now: DateTime<Utc> = Utc::now();

        format!(
            "{}/{}-{}.tar.gz",
            &settings.archive_path,
            machine_name,
            now.format(ARCHIVE_TIMESTAMP_FORMAT),
        )
    }

    fn get_relative_path(path: &str) -> String {
        let path_tokens: Vec<&str> = path.split("/").map(|v| v.into()).collect();

        path_tokens[1..].join("/")
    }
}

/// Archive Registry implements a storage policy (usually on a local storage) in which files are
/// kept some limited and configurable amount of time to prevent the storage overflow.
pub struct ArchiveRegistry {
    pub path: String,
}

impl ArchiveRegistry {
    /// Create new instance of archive registry handler.
    ///
    /// # Examples
    ///
    /// ```
    /// let registry = ArchiveRegistry::new(&settings).expect("Error");
    /// ```
    pub fn new(settings: &Settings) -> Result<Self, io::Error> {
        debug!("Initializing registry handler at: {}", &settings.archive_path);

        Ok(Self {
            path: String::from(&settings.archive_path),
        })
    }

    /// Cleanup archives that date back more than the duration specified.
    ///
    /// # Examples
    ///
    /// ```
    /// registry.cleanup(Duration::days(1)).expect("Error");
    /// ```
    pub fn cleanup(&self, max_lifetime: Duration) -> Result<(), GenericError> {
        let entries = self.get_archive_list()?;
        for entry in entries {
            let archive_lifetime = Self::get_archive_lifetime(&entry)?;
            if archive_lifetime > max_lifetime {
                info!("File is outdated, removing: {}", &entry);
                fs::remove_file(&entry)?;
            }
        }

        Ok(())
    }

    fn get_archive_list(&self) -> Result<Vec<String>, io::Error> {
        debug!("Getting archive list in: {}", &self.path);
        // warning: high geek voltage
        // https://users.rust-lang.org/t/filtering-file-names-with-ends-with/16939/4
        let entries = fs::read_dir(&self.path)?
            .filter_map(Result::ok)
            .filter_map(
                |d| d.path().to_str().and_then(
                    |f| if f.ends_with(".tar.gz") { Some(d) } else { None }
                )
            )
            .map(|res: fs::DirEntry| String::from(res.path().to_str().unwrap()))
            .collect::<Vec<String>>();

        Ok(entries)
    }

    fn get_archive_lifetime(file_path: &str) -> Result<Duration, GenericError> {
        let re = match Regex::new(r".*-([0-9]+)-([0-9]+).tar.gz") {
            Ok(v) => v,
            Err(e) => return Err(GenericError::from(&e)),
        };
        let caps = match re.captures(file_path) {
            Some(v) => v,
            None => return Err(GenericError::new(&format!(
                "Error while parsing timestamp for file path: {}",
                file_path
            )))
        };
        let timestamp_string = format!(
            "{}-{}00",
            caps.get(1).map_or("", |m| m.as_str()),
            caps.get(2).map_or("", |m| m.as_str()),
        );
        let now = Utc::now();
        let timestamp_naive = NaiveDateTime::parse_from_str(
            &timestamp_string, "%Y%m%d-%H%M%S",
        )?;
        let timestamp = DateTime::from_utc(timestamp_naive, Utc);

        Ok(now - timestamp)
    }
}

impl From<std::io::Error> for GenericError {
    fn from(err: std::io::Error) -> Self {
        GenericError::new(&format!("{}", err))
    }
}

impl From<chrono::ParseError> for GenericError {
    fn from(err: chrono::ParseError) -> Self {
        GenericError::new(&format!("{}", err))
    }
}

#[cfg(test)]
mod tests {
    mod test_archive_archive_tar_archive {
        use std::path;
        use fake::{Fake, Faker};
        use crate::core::tests::FakeSettingsFile;
        use super::super::*;

        #[test]
        fn test_archive_archive_tar_archive_new_for_machine() {
            let settings_helper = FakeSettingsFile::new(
                r#"{
                "tmp_path": "/tmp",
                "archive_tmp_path": "/tmp"
            }"#,
            );
            let settings = Settings::new(Some(&settings_helper.path)).unwrap();
            let archive = TarArchive::new_for_machine(
                &settings,
                &format!("test-guest-{}", Faker.fake::<u8>()),
            )
                .unwrap();

            assert!(path::Path::new(&archive.path).exists());

            fs::remove_file(&archive.path).unwrap();
        }

        #[test]
        fn test_archive_archive_tar_archive_add_file() {
            let settings_helper = FakeSettingsFile::new(
                r#"{
                "tmp_path": "/tmp",
                "archive_tmp_path": "/tmp"
            }"#,
            );
            let settings = Settings::new(Some(&settings_helper.path)).unwrap();
            let mut archive = TarArchive::new_for_machine(
                &settings,
                &format!("test-guest-{}", Faker.fake::<u8>()),
            )
                .unwrap();

            archive.add_file(&settings_helper.path).unwrap();

            fs::remove_file(&archive.path).unwrap();
        }

        #[test]
        fn test_archive_archive_tar_archive_get_archive_path() {
            let settings_helper = FakeSettingsFile::new(
                r#"{
                "tmp_path": "/tmp",
                "archive_tmp_path": "/tmp"
            }"#,
            );
            let settings = Settings::new(Some(&settings_helper.path)).unwrap();
            let now: DateTime<Utc> = Utc::now();
            let path = TarArchive::get_archive_path(&settings, "test-guest");

            assert_eq!(
                path,
                format!(
                    "{}/{}-{}.tar.gz",
                    &settings.archive_path,
                    "test-guest",
                    now.format("%Y%m%d-%H%M")
                )
            )
        }

        #[test]
        fn test_archive_archive_tar_archive_get_relative_path() {
            assert_eq!(
                TarArchive::get_relative_path("/absolute/path/file.txt"),
                "absolute/path/file.txt"
            )
        }
    }

    mod test_archive_archive_archive_registry {
        use std::path;
        use chrono::Duration;
        use crate::core::tests::{FakeSettingsFile, FakeMachineArchive};
        use super::super::*;

        #[test]
        fn test_archive_archive_archive_registry_new() {
            let fake_settings = FakeSettingsFile::new(
                r#"{
                    "tmp_path": "/tmp",
                    "archive_tmp_path": "/tmp"
                }"#,
            );
            let settings = Settings::new(Some(&fake_settings.path)).unwrap();
            let registry = ArchiveRegistry::new(&settings).unwrap();

            assert_eq!(&registry.path, &settings.archive_path)
        }

        #[test]
        fn test_archive_archive_archive_registry_cleanup() {
            let fake_settings = FakeSettingsFile::new(
                r#"{
                    "tmp_path": "/tmp",
                    "archive_tmp_path": "/tmp"
                }"#
            );
            let settings = Settings::new(Some(&fake_settings.path)).unwrap();
            let registry = ArchiveRegistry::new(&settings).unwrap();

            let fake_machine_1 = FakeMachineArchive::new(Utc::now() - Duration::hours(23));
            let fake_machine_2 = FakeMachineArchive::new(Utc::now() - Duration::hours(26));

            registry.cleanup(Duration::days(1)).unwrap();

            assert!(path::Path::new(&fake_machine_1.path).exists());
            assert!(!path::Path::new(&fake_machine_2.path).exists());
        }
    }
}
