use clap;
use clap_nested::{Command, Commander, MultiCommand};
use humantime;
use crate::app::Application;

pub fn get_archive_menu<'a>() -> MultiCommand<'a, str, str> {
    let archive_cmd = Command::new("cleanup")
        .description("Cleanup expired archives, that is all the archives older than the \
            value configured in the settings or passed by the user.")
        .options(|app| {
            app.arg(
                clap::Arg::with_name("after")
                    .long("after")
                    .short("a")
                    .value_name("PERIOD")
                    .help("Time span to clean up machines after"),
            )
        })
        .runner(cleanup);

    Commander::new()
        .add_cmd(archive_cmd)
        .into_cmd("archive")
        .description("Contains subcommands to manage machine archives")
}

fn cleanup(_args: &str, matches: &clap::ArgMatches<'_>) -> Result<(), clap::Error> {
    let after_arg = clap::value_t!(matches, "after", String).unwrap_or_default();
    let after = if after_arg != "" {
        match humantime::parse_duration(&after_arg) {
            Ok(v) => Some(v),
            Err(e) => {
                return Err(clap::Error::with_description(
                    &format!("Could not parse duration \"{}\": {}", &after_arg, e),
                    clap::ErrorKind::InvalidValue
                ))
            }
        }
    } else {
        None
    };

    Application::new(None)?.archive_cleanup(&after)?;

    Ok(())
}
